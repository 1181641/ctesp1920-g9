using Xunit;
using System;
using System.Collections.Generic;
using API.Models;

public class UnitTest
{
    [Fact]
    public void testValidarContaBancariaNaoExiste()
    {
        Transacao transac = new Transacao();
        //Verificar se a conta não está associada 
        Assert.False(transac.validarContaBancaria());        
    }

    [Fact]
    public void testValidarContaBancariaExiste(){

        Transacao transac = new Transacao();

        //Verificar se a conta não está associada 
        ContaBancaria conta = new ContaBancaria();
        conta.Numero_de_conta = 2;
        transac.ContaBancaria = conta;
        //Verificar se a conta está associada
        Assert.True(transac.validarContaBancaria());
    }

    [Fact]
    public void testValidarDataTransacao(){

        Transacao transac = new Transacao();

        transac.dia = DateTime.UtcNow;
        //Verificar se a data foi inserida();
        Assert.True(transac.validarData());
    }

    [Fact]
    public void testValidarValorIgualZero(){

        Transacao transac = new Transacao();

        transac.dia = DateTime.UtcNow;
        //Verificar se o valor é zero
        Assert.False(transac.validarValores());
    }

    [Fact]
    public void testValidarValorDifZero(){

        Transacao transac = new Transacao();
        transac.valor = 1.0;
        //Verificar se o valor é diferente de zero
        Assert.True(transac.validarValores());
    }

    [Fact]
    public void testValidarTipoTransacaoNaoExiste(){

        Transacao transac = new Transacao();
        Assert.False(transac.validarTipoTransacao());
    }


    [Fact]
    public void testValidarTipoTransacaoExiste(){

        Transacao transac = new Transacao();
        TipoTransacao tipo = new TipoTransacao();
        tipo.descricao = "C";
        transac.TipoTransacao = tipo;
        Assert.True(transac.validarTipoTransacao());
    }

    [Fact]
    public void testValidarValoresNegativosNumCredito(){

        Transacao transac = new Transacao();

        transac.valor = -1.0;

        TipoTransacao tipo = new TipoTransacao();
        tipo.descricao = "C";
        transac.TipoTransacao = tipo;

        Assert.False(transac.validarValoresComTipoTransacao());
    }

     [Fact]
    public void testValidarValoresPositivosNumCredito(){

        Transacao transac = new Transacao();

        transac.valor = 1.0;

        TipoTransacao tipo = new TipoTransacao();
        tipo.descricao = "C";
        transac.TipoTransacao = tipo;

        Assert.True(transac.validarValoresComTipoTransacao());
    }

    [Fact]
    public void testValidarValoresNegativosNumDebito(){

        Transacao transac = new Transacao();

        transac.valor = -1.0;

        TipoTransacao tipo = new TipoTransacao();
        tipo.descricao = "D";
        transac.TipoTransacao = tipo;

        Assert.True(transac.validarValoresComTipoTransacao());
    }

     [Fact]
    public void testValidarValoresPositivosNumDebito(){

        Transacao transac = new Transacao();

        transac.valor = 1.0;

        TipoTransacao tipo = new TipoTransacao();
        tipo.descricao = "D";
        transac.TipoTransacao = tipo;

        Assert.False(transac.validarValoresComTipoTransacao());
    }

}