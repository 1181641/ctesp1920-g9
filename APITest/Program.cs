﻿using System;

namespace APITest
{
    class Program
    {
        static void Main(string[] args)
        {
            UnitTest test = new UnitTest();
            test.testValidarContaBancariaNaoExiste();
            test.testValidarContaBancariaExiste();

            test.testValidarDataTransacao();

            test.testValidarValorIgualZero();
            test.testValidarValorDifZero();

            test.testValidarTipoTransacaoNaoExiste();
            test.testValidarTipoTransacaoExiste();

            test.testValidarValoresNegativosNumCredito();
            test.testValidarValoresPositivosNumCredito();

            test.testValidarValoresNegativosNumDebito();
            test.testValidarValoresPositivosNumDebito();
            
        }
    }
}
