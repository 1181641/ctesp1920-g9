﻿using API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace API.Infrastructure
{
    public class DbApiContext : DbContext
    {
        public DbApiContext(DbContextOptions<DbApiContext> options) : base(options){
            
        }

        public DbSet<Cliente> clientes { get; set; }
        public DbSet<ContaBancaria> contas { get; set; }
        public DbSet<TipoTransacao> tipos { get; set; }
        public DbSet<Transacao> transacoes {get; set;}

        private static void AddTestData(DbApiContext context){

            List<Cliente> newclientes = new List<Cliente>();
            List<ContaBancaria> newcontas = new List<ContaBancaria>();
            List<TipoTransacao> newtipos = new List<TipoTransacao>();
            List<Transacao> newtransacoes = new List<Transacao>();

            newclientes.Add(new Cliente
            {
                clienteId=1,
                Nome_Cliente="Tiago Santos",
                Morada="Praceta Padre 2, 2ºesq",
                Contacto="912665307"
            });
            newcontas.Add(new ContaBancaria{
                contaId=1,
                Numero_de_conta=12,
                IBAN="0013 1234 2193 1234 9",
                Saldo_Corrente=19.0,
                clienteId=1
            });

            newtransacoes.Add(new Transacao{
                transacaoId=1,
                dia=DateTime.UtcNow.AddDays(-10),
                valor=15.0,
                contaId=1,
                tipoId=2
            });
            newtransacoes.Add(new Transacao{
                transacaoId=2,
                dia=DateTime.UtcNow.AddDays(-10),
                valor=-5.0,
                contaId=1,
                tipoId=1
            });

            newtransacoes.Add(new Transacao{
                transacaoId=3,
                dia=DateTime.UtcNow.AddDays(-9),
                valor=10.0,
                contaId=1,
                tipoId=2
            });
            newtransacoes.Add(new Transacao{
                transacaoId=4,
                dia=DateTime.UtcNow.AddDays(-9),
                valor=-5.0,
                contaId=1,
                tipoId=1
            });

            newtipos.Add(new TipoTransacao{
                tipoId=1,
                descricao="D"
            });
            newtipos.Add(new TipoTransacao{
                tipoId=2,
                descricao="C"
            });

            context.clientes.AddRange(newclientes);
            context.contas.AddRange(newcontas);
            context.tipos.AddRange(newtipos);
            context.transacoes.AddRange(newtransacoes);
            context.SaveChanges();
        }
        


    }
    

}
