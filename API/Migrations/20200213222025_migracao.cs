﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class migracao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "clientes",
                columns: table => new
                {
                    clienteId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome_Cliente = table.Column<string>(nullable: true),
                    Morada = table.Column<string>(nullable: true),
                    Contacto = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_clientes", x => x.clienteId);
                });

            migrationBuilder.CreateTable(
                name: "tipos",
                columns: table => new
                {
                    tipoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tipos", x => x.tipoId);
                });

            migrationBuilder.CreateTable(
                name: "contas",
                columns: table => new
                {
                    contaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Numero_de_conta = table.Column<int>(nullable: false),
                    IBAN = table.Column<string>(nullable: true),
                    Saldo_Corrente = table.Column<double>(nullable: false),
                    clienteId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_contas", x => x.contaId);
                    table.ForeignKey(
                        name: "FK_contas_clientes_clienteId",
                        column: x => x.clienteId,
                        principalTable: "clientes",
                        principalColumn: "clienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transacoes",
                columns: table => new
                {
                    transacaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dia = table.Column<DateTime>(nullable: false),
                    valor = table.Column<double>(nullable: false),
                    contaId = table.Column<int>(nullable: false),
                    tipoId = table.Column<int>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transacoes", x => x.transacaoId);
                    table.ForeignKey(
                        name: "FK_transacoes_contas_contaId",
                        column: x => x.contaId,
                        principalTable: "contas",
                        principalColumn: "contaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_transacoes_tipos_tipoId",
                        column: x => x.tipoId,
                        principalTable: "tipos",
                        principalColumn: "tipoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_contas_clienteId",
                table: "contas",
                column: "clienteId");

            migrationBuilder.CreateIndex(
                name: "IX_transacoes_contaId",
                table: "transacoes",
                column: "contaId");

            migrationBuilder.CreateIndex(
                name: "IX_transacoes_tipoId",
                table: "transacoes",
                column: "tipoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "transacoes");

            migrationBuilder.DropTable(
                name: "contas");

            migrationBuilder.DropTable(
                name: "tipos");

            migrationBuilder.DropTable(
                name: "clientes");
        }
    }
}
