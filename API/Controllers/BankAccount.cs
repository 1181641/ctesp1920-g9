using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Infrastructure;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers 
{
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class BankAccount : ControllerBase 
    {

        private readonly DbApiContext _context;
        public BankAccount(DbApiContext context){
            _context = context;
            
        }

        [HttpGet("{id}")]
            public IQueryable Get(int? id)
            {
                return _context.contas.Where(bc => bc.contaId == id).Select(bc => new
                {
                    Nome_do_Titular = bc.Cliente.Nome_Cliente,
                    Morada = bc.Cliente.Morada,
                    Contacto = bc.Cliente.Contacto,
                    Saldo_Atual = bc.Saldo_Corrente,
                });
            
        }
        [HttpGet("{id}/transaction")]
        public IActionResult  GetTransaction(int? id)
        {
            double totalCredito, totalDebito, soma;
            totalCredito = _context.transacoes.Where(x => x.contaId == id && x.tipoId==2).Sum(x => x.valor);
            totalDebito = _context.transacoes.Where(x => x.contaId == id && x.tipoId==1).Sum(x => x.valor);
            soma = totalCredito + totalDebito;

            var EndOfDayBalances = new[] { new { Date = DateTime.UtcNow, Balance = 100.50 } }.ToList();
            EndOfDayBalances.Clear();

            double balanco = soma;
            (from s in _context.transacoes
                where (s.contaId == id && s.dia >= DateTime.Now.AddMonths(-1))
                group s by new { date = new DateTime(s.dia.Year, s.dia.Month, s.dia.Day) } into g
                select new {
                    Date = g.Key.date,
                    Balance = g.Sum(x => x.valor)
                }).ToList().OrderByDescending(o => o.Date).All(trans => {
                    EndOfDayBalances.Add( new { Date = trans.Date, Balance = balanco });
                    balanco -= trans.Balance;
                    return true;
                });

            return Ok(new {
                TotalCredits=totalCredito,
                TotalDebits=totalDebito,
                EndOfDayBalances=EndOfDayBalances
            });
        }

        [HttpPost]
        public async Task<ActionResult<Transacao>> Add(Transacao transac){

            if (transac == null){
                return BadRequest();
            }

            try{

                if(transac.isValid()){

                    if (_context.contas.Where(x => x.Numero_de_conta == transac.ContaBancaria.Numero_de_conta).Any()){
                        transac.contaId =  _context.contas.Where(x => x.Numero_de_conta == transac.ContaBancaria.Numero_de_conta).Select(x => x.contaId).FirstOrDefault();
                        transac.ContaBancaria = null;
                    }else{
                        return BadRequest();
                    }

                    if (_context.tipos.Where(x => x.descricao == transac.TipoTransacao.descricao).Any()){
                        transac.tipoId =  _context.tipos.Where(x => x.descricao == transac.TipoTransacao.descricao).Select(x => x.tipoId).FirstOrDefault();
                        transac.TipoTransacao = null;
                    }else{
                        return BadRequest();
                    }
                    await _context.transacoes.AddAsync(transac);
                    await _context.SaveChangesAsync();

                    double saldoCorrente = _context.transacoes.Where(x => x.contaId == transac.contaId).Sum(x => x.valor);
                    ContaBancaria conta = _context.contas.Where(x => x.contaId ==transac.contaId).FirstOrDefault();
                    conta.Saldo_Corrente = saldoCorrente;
                    _context.SaveChanges();

                    var response = new
                    {
                        resultado = "Inserido"
                    };
                    return Ok(response);
                }else{
                    return BadRequest();
                }

            }catch(Exception){
                return BadRequest();
            }

            
        }
    }
}

