using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace API.Models
{
    public class TipoTransacao 
    {
        [Key]
        public int tipoId {get; set;}
        public string descricao {get; set;}
    }
}