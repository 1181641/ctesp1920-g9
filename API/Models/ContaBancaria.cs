using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace API.Models
 {
    public class ContaBancaria
    {
        [Key]
        public int contaId {get; set;}
        public int Numero_de_conta {get; set;}
        public string IBAN {get; set;}
        public double Saldo_Corrente {get; set;}
        public int clienteId {get; set;}
        [ForeignKey("clienteId")]
        public virtual Cliente Cliente {get; set;}

    }
}
