using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace API.Models 
{
    public class Transacao
    {
        public Transacao()
        {
            this.transacaoId = 0;
            this.dia = DateTime.Now;
            this.valor = 0.0;
        }

            [Key]
            public int transacaoId {get; set;}
            public DateTime dia {get; set;}
            public double valor {get; set;}

            public int contaId {get; set;}
                [ForeignKey("contaId")]
            public virtual ContaBancaria ContaBancaria {get; set;}

            public int tipoId {get; set;}
                [ForeignKey("tipoId")]
            public virtual TipoTransacao TipoTransacao {get; set;}

            public Boolean isValid()
            {
                //Validar a conta bancaria
                if (!validarContaBancaria()){
                    return false;
                }

                //Validar data da transação
                if (!validarData()){
                    return false;
                }

                //Validar valores inseridos na transação.
                if (!validarValores()){
                    return false;
                }

                //Validar tipo de Transação
                if(!validarTipoTransacao()){
                    return false;
                }
                
                // Validar valores consoante o tipo de transação
                if (!validarValoresComTipoTransacao()){
                    return false;
                }

                return true;
            }

            public Boolean validarContaBancaria(){
                if (this.ContaBancaria == null){
                    return false;
                }
                if (this.ContaBancaria.Numero_de_conta == 0)
                {
                    return false;
                }
                return true;
            }

            public Boolean validarData(){
                if (this.dia == null){
                    return false;
                }
                return true;
            }

            public Boolean validarValores(){
                if (this.valor == 0.0){
                    return false;
                }
                return true;
            }

            public Boolean validarTipoTransacao(){
                if(this.TipoTransacao==null){
                    return false;
                }
                if (this.TipoTransacao.descricao == null || this.TipoTransacao.descricao == ""){
                    return false;
                }
                return true;
            }

            public Boolean validarValoresComTipoTransacao(){
                if ((this.TipoTransacao.descricao == "C" && this.valor<=0) || (this.TipoTransacao.descricao == "D" && this.valor>=0)){
                    return false;
                }
                return true;
            }
        }
    }

